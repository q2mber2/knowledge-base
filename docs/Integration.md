# Integration

### Download modules files


<!--
type: tab
title: Web
-->

## Web

For our modules we utilize Angular elements approach. Angular elements are Angular components packaged as custom elements (also called Web Components), a web standard for defining new HTML elements in a framework-agnostic way. [Read more](https://angular.io/guide/elements)


Copy resulting build to your project's static folder and load it to your index.html

<!--
title: "My code snippet"
lineNumbers: true
-->

```javascript
var script = document.createElement('script');
var element;
script.src = `{PATH}/optherium-kyc.js`;
script.id = 'kyc-script';
script.onload = function () {
    element = document.createElement(`optherium-kyc`);
    element.id = 'kyc-element';
    container.appendChild(element);
    resolve(element);
    script.removeEventListener('onload', function () {
        console.warn(`${module.toUpperCase()} onload listener removed`);
        });
    };
document.head.appendChild(script);
```

Subscribe on complete event to know when user has completed KYC/login scenario and redirect him to your app.

<!--
title: "My code snippet"
lineNumbers: true
-->

```javascript
element.addEventListener('complete', async () => {
    // do something
})
```

<!--
type: tab
title: Android
-->

## Android

With more mind-blowing material. Really. Just amazing, grade-A stuff.

<!--
type: tab
title: IOS
-->

## IOS

With more mind-blowing material. Really. Just amazing, grade-A stuff.

<!-- type: tab-end -->


## Server-side integration

The following are described most applicable use-cases for server-side integration.

### User authentication 

![Registration](../assets/images/Integration_diagram.png)

User authentication is based on JWT([learn more](https://jwt.io/introduction/)) Sequance diagram describes authentication procedure. To establish connection with KYC & Identity Management Network user requests a token. This token contains information about user's identity and device as well as user's session. If user's session is expired he requests authentication(one of proposed methods). On success authentication user recieves new token with active session. User requests Application Server to request private resource. Application Server validates token and based on a result authorizes a user to a resource. 




[API Methods](reference/Server-API.v1.json)


### Strong Customer Authentication (SCA)

![Registration](../assets/images/SCA.png)

You can use the same methods as for sign-in for Strong Customer Authentication. When a user requests an operation required SCA, your server application should generate a unique ```Operation ID```. A user performs authentication providing ```Operation ID``` and on success event confirms operation on your server. Your application server verifies SCA using the same ```Operation ID``` and finally performs an operation.

[API Methods](reference/Server-API.v1.json)

### Pulling data

This is the most common scenario. You can pull any data stored in the network to use it in your application

<!-- theme: warning -->

> ### Watch Out!
>
> By default any data is available to download. But to minimize data breach risks we reccomend to narrow these permissions as much as possible and follow data minimization principle ([article 5(c) GDPR](https://eur-lex.europa.eu/eli/reg/2016/679/oj#d1e1807-1-1)) . If you use microservice architecture and blockchaing integration you can also configure different set of permissions for different services.

[API Methods](reference/Server-API.v1.json)
