# Key concepts

### Registration

Basic registration requires verification of only one method. By default, it's a phone number, but it's possible to use any other authentication method for this purpose(e.g., email). After successful registration Identity asset and Device asset bind to Identity are created, and a user can continue with the next steps.

![Registration](../assets/images/registration.gif)

### KYC(Know Your Customer)

During the KYC procedure, a user provides images of identification documents and selfie(Document Verification), his address with images containing proof of this address(Address Verification) and, email address. All steps are configurable. It's possible to change an order, make steps optional or mandatory, add custom steps to gather additional data only your business needs. 

![KYC](../assets/images/KYC.gif)


### New Device Authorization(binding)

The user can bind multiple devices to his Identity. To bind a new device user performs a registration first, then sends a request and approves it from any authorized device. The user can manage a list of authorized devices and remove devices when needed.

![Device Authorization](../assets/images/device_auth.gif)


### Strong Customer Authentication

In addition to authentication by a private key, the user can use verified during the KYC methods such as phone, email, document to authorize sensitive operations: financial transactions, new session, change of critical user preferences

![Device Authorization](../assets/images/SCA.gif)

### Stand-alone Authentication App

Authentication App is a more advanced alternative to existing MFA alternatives. It provides authentication, session, and device management functions. It also can act as a remote controller to terminate your sessions and lock your desktop if you left your workstation and forgot to do that.

![Authentication App](../assets/images/auth_app.gif)

### Recovery

When a user has lost access to all his authorized devices and wants to recover access to his account(Identity).  The user has to pass the same steps, passed during the registration, to recover access to his Identity. On successful recovery request, all previous Devices will be removed from Identity and replaced with the current Device.

# Flow chart

The diagram below illustrates the default logic of the module. It should help you better understand underlying processes and how they can be customized.
Some bullet points:

- When the application launches it initializes the SDK and calls getState to check whether a device is registered or not.
- If the device is already registered, the application redirects the user to the login page or directly to the uncompleted KYC stage.
- If the device doesn’t have any state, the application asks the user for a phone number registration
- If the phone number is not registered in the system user should go to the beginning of the KYC procedure.
- If the phone number is registered in the system user has two options: New Device Authorization or Recovery

![business logic](../assets/images/businesslogic.png)

