# DataVault and GDPR

## Terms

**Personal data** is any information relating to an identified or identifiable natural person. 
									
One of the key aspects of the GDPR is that it aims to create consistency across EU member states on how personal data can be processed, used, and exchanged securely. 
				
### Controller
The controller is the natural person or legal entity that determines the purposes and means of the processing of personal data
The key responsibility of a controller is to be accountable, i.e., to take actions in line with GDPR, and to be able to explain the compliance with GDPR to data subjects and the Supervisory Authority, as and when required.


### Processor
A natural person or legal entity that processes personal data on behalf of the controller (e.g., call centers acting on behalf of its client) is considered to be a processor. At times, a processor is also called a third party.
The key responsibility of the processor is to ensure that conditions specified in the Data Processing Agreement signed with the controller are always met and that obligations stated in GDPR are complied with.

### Supervisory Authority
A Supervisory Authority is a public authority in an EU country responsible for monitoring compliance with GDPR.
The key role of the Supervisory Authority is to advise companies about GDPR, conduct audits on compliance with GDPR, address complaints from data subjects, and issue fines when companies are deliberately not complying with GDPR.







## Requirements

### Defined rules and purposes		
Article 25 of the GDPR states that the controller “shall implement appropriate technical and organizational measures for ensuring that, by default, only personal data which are necessary for each specific purpose of the processing are processed.” 
					
*Connector blockchain network defines rules and purposes how data is stored and retrieved as a smart contract. It has its own Certificate Authority which issues and revokes certificates for all participants on the network. Validation happens on multiple peers shared between all Sensitive Storage Modules (Peer Sharing)*

### Log of all operations			
The GDPR requires that “[e]ach controller and, where applicable, the controller’s representative, shall maintain a record of processing activities under its responsibility.” This article also includes details of the information that needs to be recorded. In other words, the GDPR requires monitoring of the processing of PII data. In addition, timely breach notification obligations require that incidents are detected in almost real-time. 

*All logs are saved on multiple distributed ledgers:*

*Connector blockchain network contains all operations(create, read, update, delete) with data made by other actors(users, processors, other connectors)
Processor blockchain networks contain logs of all operations between Connector service and each Sensitive Storage Module in a cluster*

*All these channels can be shared with trusted Supervisory Authority*

### Encryption and pseudonymisation			 			
The GDPR requires that organizations must “implement appropriate technical and organizational measures to ensure a level of security appropriate to the risk, including (...) the pseudonymization and encryption of personal data (...).” In addition, organizations must safeguard against the unauthorized disclosure of, or access to, personal data. 


- pseudonymization is achieved by splitting PII between multiple processors
- each processor encrypts data with its own private key using AES256 standard


### Notify on data breach						
Finally, where a personal data breach has occurred and is likely to result in a high risk to the rights and freedoms of natural persons, but the controller has put in place “appropriate technical and organizational protection measures (...) such as encryption”, the controller needs to notify the affected data subjects of the breach, and can, therefore, avoid administrative costs and reputational damage. 		
	
*Using DLP systems with Distributed Ledger Technology with and sharing ledgers with Supervisory Authority provides a realtime, transparent and auditable monitoring tool*

### Data portability
Essentially data portability is the right to transfer personal data from one organization (controller) to another organization or to the data subject in the context of digital personal data (sets and subsets)and automated processing

*PII doesn’t belong to any controller or processor. Сonversely Data Storage Processor can be configurable to work with other processors and controllers. Also, data is always accessible by a user, he can retrieve it and provide it to a non-ecosystem connector.* 
 
### Right to be Forgotten

We don’t store PII data on a blockchain, so it can be removable. Conditions about how and when data can be removed from a processor are also defined in a Connector blockchain network.


