# KYC NODE SDK

## Installation

All Optherium source codes are accessible by Client TOKEN. You should receive it from Optherium Sales Team. Just replace TOKEN in the lines below to clone the repository.

You can clone the repository

```
git clone TOKEN:x-oauth-basic@github.com/Optherium/kyc-node-sdk

```

## Config

``` typescript
type IConfig = {
    clientName: string,
    caName: string,
    orgName: string,
    connectionProfile: {
        name: string,
        version: string,
        peers: Record<string, {
            url: string;
            tlsCACerts: {
                pem: string
            }
        }>,
        organizations: Record<string, {
            mspid: string;
        }>,
        channels: Record<string, {
            peers: Record<string, {
                endorsingPeer: boolean,
                chaincodeQuery: boolean,
                ledgerQuery: boolean,
                eventSource: boolean,
                discover: boolean
            }>,
        }>
    },
    credentials: {
        enrollmentID: string,
        certificate: string,
        privateKey: string
    },
    blockchain: {
        kyc: {
            channelId: string,
            chaincodeId: string
        },
        read: {
            channelId: string,
            chaincodeId: string
        }[]
    }
    lnm: {
        url: string // address of local network manager
    }
    tls: boolean
}

```

## Initialization
SDK contains the API method.
ServiceDiscoveryProvider collects information about channel and chaicnode from a peer.
This information will be used further for communication with blockchain. 
TxObserver watches transaction state from peers. 
Local Network Manager is a blockchain network manager. It creates channels, chaincodes, organizations, identities. 


``` typescript
import { SDK, GetConnectionProfile,
 ServiceDiscoveryProvider, TxObserver } from'kyc-node-sdk';
const config: IConfig = config;
const connectionProfile = await GetConnectionProfile({
  lnm: config.lnm,
  channelsIds: [
  config.blockchain.kyc.channelId,
  config.blockchain.read['1'].channelId,
  config.blockchain.read['2'].channelId,
  config.blockchain.read['3'].channelId
  ]
});
config.connectionProfile = connectionProfile;
const serviceDiscoveryProvider = new ServiceDiscoveryProvider({
      clientName: config.credentials.enrollmentID,
      orgName: config.orgName,
      credentials: config.credentials,
      connectionProfile: config.connectionProfile,
      tls: config.tls
      });
await serviceDiscoveryProvider.start({});
const sdk = new SDK(config);
const txObserver = new TxObserver({
    clientName: config.credentials.enrollmentID,
    orgName: config.orgName,
    credentials: config.credentials,
    connectionProfile: config.connectionProfile,
    tls: config.tls
});
await txObserver.startObserver();
await this.sdk.init(txObserver, serviceDiscoveryProvider);
```

## SDK API structure

``` typescript
interface ISDK {
    files: IFiles;
    init(txObserver: fabricConnector.TxObserver, serviceDiscoveryProvider: fabricConnector.ServiceDiscoveryProvider): Promise<void>;
}
```
## SDK Files API
  **getPhone(identityId: string): **returns indentity phone number.

  **getEmail(identityId: string):** returns indentity email address.

  **getDocumentInfo(identityId: string):** returns parsed document information from indentity identification document (passport, driver license etc).
   
  **getDocumentImages(identityId: string):** returns identification document pictures base64 encoded.

   **getAddressInfo(identityId: string)**: returns parsed information provided by identity for address verification by utility bill

 **getAddressImages(identityId: string):** returns utility bill pictures base64 encoded.

  **getBioInfo(identityId: string):** returns parsed bio information from indentity identification document (passport, driver license etc).

   **getBioInfo(identityId: string):** returns parsed due diligence information provided by indentity.
   

``` typescript
interface IFiles {
    getPhone(identityId: string): Promise<IGetPhoneReply>;
    getEmail(identityId: string): Promise<IGetEmailReply>;
    getDocumentInfo(identityId: string): Promise<IGetDocumentInfoReply>;
    getDocumentImages(identityId: string): Promise<IGetDocumentImagesReply>;
    getAddressInfo(identityId: string): Promise<IGetAddressInfoReply>;
    getAddressImages(identityId: string): Promise<IGetAddressImagesReply>;
    getBioInfo(identityId: string): Promise<IGetBioInfoReply>;
    getDueDiligenceInfo(identityId: string): Promise<IGetDueDiligenceInfoReply>;
}

type IGetPhoneReply = {
    phone: string
}

type IGetEmailReply = {
    email: string
}

type IGetDocumentInfoReply = {
    info: string
}

type IGetDocumentImagesReply = {
    frontImage: string,
    backImage: string,
    faceImage: string,
}

type IGetAddressInfoReply = {
  info: {
      address: string,
      city: string,
      number: string,
      streetType: string,
      country: string,
      region: string,
      street: string,
      buildingNumber: string,
      postalCode: string,
  }
}

type IGetAddressImagesReply = {
    images: string[]
}

type IGetBioInfoReply = {
  info: {
      Name: string,
      Surname: string,
      Nationality: string,
      DateOfBirth: string,
      PlaceOfBirth: string,
      Sex: string
  }
}

type IGetDueDiligenceInfoReply = {
  info: {
      occupation: string,
      area: string,
      card_purpose: string,
      monthly_load: string,
      monthly_spend: string,
      monthly_income: string,
      estate: string,
  }
}
```





