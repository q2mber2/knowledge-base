# Customization

Native mobile and web UI modules will help you to integrate our solutions into your business in hours. Modules support a wide range of customization to make it
look unique and fit the tone of voice of your business.
We provide a big variety of styles and functionality customization for your brand purposes.

![Custom design](../assets/images/colors-examples.png)


## 1. Download configuration file

Before you begin you can download the configuration file. Please follow the proposed structure and formats for your custom configuration to avoid conflicts.
[Download]() 

## 2. Style Configuration
```/style``` directory includes basic configuration for:
1. Logos
2. Colors
3. Fonts

<!-- theme: warning -->

> ### Style Guide
>
> Please download and provide this guide to your design team. It should give a better understanding of basic UI elements, where and how colors are used, what is available for customization. [Download]() 


## 3. Localizaton
![Custom design](../assets/images/Custom_design.png)

You can edit text and add custom translations to other languages. Open ```/translation``` directory. 

<!-- theme: warning -->

> ### To add your translation
>
> You should follow ```en.json``` file structure and use exactly the same object keys. Be careful, some strings accept parameters enclosed with brackets ```{ value }```.

<!-- theme: warning -->

> ### To modify a translation
>
> Don't put your modifications into default files. It'll make migration to new versions very difficult. Instead of that describe the only strings you want to modify and put into ```/translations/override``` directory



