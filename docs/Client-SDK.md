# Client SDK 

The Optherium SDK is designed to allow access to all of the features of the Optherium platform, subject to you complying with the API Terms of Service. The end goal is to allow people to recreate the entire platform on their own.

We can integrate our solutions using our customizable user interface modules for Web, IOS, and Android. If you are looking for this solution go to the section …

We recommend you to use Model-View-Controller(MVC) approach in your application, where SDK acts as a Model(business logic) and Controller by emitting state events subscribing on which you can change the View. More about state management you can read in the section …

SDK implements the same interface for JavaScript, Android, and iOS. Below you can find a description of methods of JavaScript implementation.



## Installation

All Optherium source codes are accessible by Client TOKEN. You should receive it from Optherium Sales Team. Just replace TOKEN in the lines below to clone the repository.

You can clone repository

```
git clone TOKEN:x-oauth-basic@github.com/Optherium/optherium-sdk-integration-angular
```



## Storage

Optherium SDK uses long-term storage to store private keys, certificates, state data, and other data required for its work. Storage realization should implement the securest way to store information as possible for a platform (KeyChain for iOS, encrypted storage for Android)

<!--
type: tab
title: Web
-->
``` javascript
interface IStorage {
    setItem(key: string, value: string): Promise;
    removeItem(key: string): Promise;
    getItem(key: string): Promise;
    getKeys(): Promise;
}
```

<!--
type: tab
title: Android
-->
## TODO

<!--
type: tab
title: IOS
-->

## TODO

<!-- type: tab-end -->

## Config

If you are running the cloud version of the platform just use a config file provided by Optherium Team. If you are running the decentralized version with a client infrastructure run on your end, you need to fill a config file with your settings. Read more information on the Network Manager section



<!--
type: tab
title: Web
-->
``` javascript
// The basic interface looks as follows:
interface IConfig {
    wsConnection: IWsConnectionConfig;
    authService: IAuthServiceConfig;
    appHost: string;
    dvsChunks: IDvsChunkConfig[];
}

// Connection Options to publicly available Client API Gateway service, connected to the blockchain peers

interface IWsConnectionConfig {
    url: string;
    path: string;
    multiplex: boolean;
    ackTimeout: number;
}

// Connection options to Authentification service, connected to CA

interface IAuthServiceConfig {
    host: string;
    loginPath: string;
    logoutPath: string;
    refreshPath: string;
}

// Connection options to Data Vault for sensitive and personal information

interface IDvsChunkConfig {
  holderMSPID: string;
  holderAff: string;
  postUrl: string;
  getUrl: string;
}

```



<!--
type: tab
title: Android
-->
## TODO

<!--
type: tab
title: IOS
-->

## TODO

<!-- type: tab-end -->



## Initialization

Code below initializes Optherium SDK and shows how it can be used. Remember that you should provide your realization of IStorage interface to the constructor. After initialization, you can call actions and subscribe on state change to receive a result of an operation and update the user interface


<!--
type: tab
title: Web
-->
``` javascript
import {SDKCore, FlowService, StateFlow} from 'optherium-sdk'

let sdk = new SDKCore(new Storage(), config)
sdk.init()
let flow = FlowService.submitPhone(phone)
sdk.state.subscribe(state => {
    switch (state.flow) {
        case StateFlow.REGISTRATION_PHONE_SUCCESS:
            //do something
            break;
    }
})
```

<!--
type: tab
title: Android
-->
## TODO

<!--
type: tab
title: IOS
-->

## TODO

<!-- type: tab-end -->


## SDKCore





