# Introduction

KYC & Identity Management module is a comprehensive and customizable tool that takes care of customer onboarding and authentication as well as regulatory compliance and secure storage of personal data.

### Passwordless environment

Provide your users with the best experience and security based on Public Key Infrastructure(PKI). Every action user does is signed with a unique private key that never leaves the user's devices. 

[Learn more](passwordless.md)


### Strong Customer Authentication(SCA)

User's private keys are additionally protected with different authentication methods(Biometric authentication, push codes, SMS, email) Developers can seamlessly integrate SCA into their applications to protect sensitive endpoints. 

### Secure and compliant data storage

The platform is based on patented DataVault technology designed for unprecedented security and privacy. DataVault meets GDPR requirements of strict rules and purposes of processing data, encryption, pseudo anonymization, and logging.

[Learn more](DataVault-and-GDPR.md)

### Customizable UI Modules

Native mobile and web modules are written on Swift, Kotlin, and JavaScript support a wide range of customization: logic, appearance, and tailor-made features only your business needs. Modules are framework agnostic and can be integrated into an application written in any language.   

[Learn more](UI-Modules.md)

### Websockets API and SDK

For all our products, we use WebSockets and Reactive Programming paradigm. It means all our applications are truly live, and all actions are instant and simultaneously synchronized between all devices user has providing the best user experience. For a better developer experience, we provide a high-level abstraction over cryptographic operations and API calls called Optherium SDK.

[Learn more](Client-SDK.md)




