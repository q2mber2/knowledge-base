# Passwordless environment

Everybody knows that login-password authentication is weak. People usually ignore all security recommendations and use one password for all services they use, nobody changes it every few months. People are used to use passwords that are easy to remember which usually means it's easy to hack. Recovering a password sometimes is very tricky.

On the other hand, there is the PKI(Public Key Infrastructure) often it's a synonym for electronic signature. Various cryptocurrencies also use this common cryptography algorithm to verify users' transactions. But out of the box PKI provides better security but lower user experience and in non-professional hands, it might be even more vulnerable. You can find a lot of stories over the internet about stolen or lost private keys.

In our solution, we took the best - the usability of login-password authentication and the security of PKI. we solve usability issues of PKI like recovery of lost keys and management of multiple certificates belonging to one person. Users even don't suspect that they use enhanced cryptography mechanisms, everything is managed under the hood.

Besides that, we protect users’ private keys with different authentication methods. Like biometric login, push codes, SMS, and emails OTP codes. Thus there are always at least 2 different authentication methods used.

All of these are a part of our KYC and Identity Management platform which acts as a decentralized certificate authority on a blockchain. Different services verify different aspects of a user’s personal data: phone number, document, email. If one service is compromised it doesn’t lead to any consequences. While in centralized CA compromisation of a private key of CA has serious consequences

